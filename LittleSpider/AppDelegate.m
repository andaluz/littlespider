//
//  AppDelegate.m
//  LittleSpider
//
//  Created by Nordin Bouchtaoui on 08/07/2017.
//  Copyright © 2017 Nordin Bouchtaoui. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.listOfLocationUpdates = [[NSMutableArray alloc] init];
    [ self startSignificantChangeLocationUpdates ];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma Location handling

-(void)startSignificantChangeLocationUpdates {
    // Create a location manager object
    self.locationManager = [[CLLocationManager alloc] init];
    
    // Set the delegate
    self.locationManager.delegate = self;
    
    // Request location authorization
    [self.locationManager requestAlwaysAuthorization];
    
    // Start significant-change location updates
    [self.locationManager startMonitoringSignificantLocationChanges];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    NSLog(@"didUpdateLocations called");
    // Perform location-based activity
    //...
    CLLocation * currentLocation = [locations lastObject];
    NSString * latitude = [NSString stringWithFormat:@"%.6f", currentLocation.coordinate.latitude];
    NSString * longitude = [NSString stringWithFormat:@"%.6f", currentLocation.coordinate.longitude];
    NSString *newLoc = [NSString stringWithFormat: @"Lat: %@ - Lon: %@", latitude, longitude];
    [self.listOfLocationUpdates addObject: newLoc];
    
    // Stop significant-change location updates when they aren't needed anymore
    //[self.locationManager stopMonitoringSignificantLocationChanges];
}


@end
