//
//  AppDelegate.h
//  LittleSpider
//
//  Created by Nordin Bouchtaoui on 08/07/2017.
//  Copyright © 2017 Nordin Bouchtaoui. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) NSMutableArray *listOfLocationUpdates;


@end

