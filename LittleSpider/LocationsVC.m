//
//  LocationsVC.m
//  LittleSpider
//
//  Created by Nordin Bouchtaoui on 09/07/2017.
//  Copyright © 2017 Nordin Bouchtaoui. All rights reserved.
//

#import "LocationsVC.h"
#import "AppDelegate.h"

@interface LocationsVC () {
    NSMutableArray *locationList;
}

@end

@implementation LocationsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    locationList = ((AppDelegate*)[[UIApplication sharedApplication] delegate]).listOfLocationUpdates;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return locationList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [locationList objectAtIndex: indexPath.row];//[NSString stringWithFormat: @"%@ - %lu", @"Test", indexPath.row];//[tableData objectAtIndex:indexPath.row];
    
    return cell;
}


@end
