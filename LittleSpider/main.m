//
//  main.m
//  LittleSpider
//
//  Created by Nordin Bouchtaoui on 08/07/2017.
//  Copyright © 2017 Nordin Bouchtaoui. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
